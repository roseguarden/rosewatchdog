from git import Repo, exc
from flask import Flask
import shutil
import os
import time
import json
import datetime
import logging
from xmlrpc.client import ServerProxy
from update import updateRoseupdate, updateRoseguarden
from repoTools import is_git_repo, get_or_create_repo

app = Flask(__name__)

base_local_repo_path = '/home/roseguarden/'
base_remote_repo_url = 'https://gitlab.com/roseguarden/'
supervisor_url = 'http://localhost:9001/RPC2'


def setup():
    try:
        repo_backend = get_or_create_repo(base_remote_repo_url + 'roseguarden.backend', base_local_repo_path + 'repositories/roseguarden.backend')
        repo_frontend = get_or_create_repo(base_remote_repo_url + 'roseguarden.frontend', base_local_repo_path + 'repositories/roseguarden.frontend')
        repo_mockup = get_or_create_repo(base_remote_repo_url + 'rosenodemock', base_local_repo_path + 'repositories/rosenodemock')
        repo_mockup_frontend = get_or_create_repo(base_remote_repo_url + 'rosenodemock.frontend', base_local_repo_path + 'repositories/rosenodemock.frontend')
        return repo_backend, repo_frontend, repo_mockup, repo_mockup_frontend
    except Exception as e:
        print("failed to setup repositories ({}) ".format(e))
        exit(1)

def check_for_updates(repo_backend, repo_frontend, repo_mockup, repo_mockup_frontend):
    actual_backend_master_hash = ""
    pulled_backend_master_hash = ""
    old_pulled_backend_master_hash = ""

    actual_frontend_master_hash = ""
    pulled_frontend_master_hash = ""
    old_pulled_frontend_master_hash = ""

    actual_mockup_master_hash = ""
    pulled_mockup_master_hash = ""
    old_pulled_mockup_master_hash = ""

    actual_mockup_frontend_master_hash = ""
    pulled_mockup_frontend_master_hash = ""
    old_pulled_mockup_frontend_master_hash = ""

    need_update = True

    while(True):
        try:
            # pull origins of backend
            backend_remote = repo_backend.remotes.origin
            backend_remote.pull()

            # pull origins of frontend
            frontend_remote = repo_frontend.remotes.origin
            frontend_remote.pull()

            # pull origins of mockup
            mockup_remote = repo_mockup.remotes.origin
            mockup_remote.pull()

            # pull origins of mockup
            mockup_frontend_remote = repo_mockup_frontend.remotes.origin
            mockup_frontend_remote.pull()

            # get actual hashes
            pulled_backend_master_hash = repo_backend.commit('master').hexsha
            pulled_frontend_master_hash = repo_frontend.commit('master').hexsha
            pulled_mockup_master_hash = repo_mockup.commit('master').hexsha
            pulled_mockup_frontend_master_hash = repo_mockup_frontend.commit('master').hexsha

            # print out for new hash for roseguarden.backend
            print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), " Check master origin for backend", pulled_backend_master_hash)
            old_pulled_backend_master_hash = pulled_backend_master_hash

            # print out for new hash for roseguarden.frontend
            print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), " Check master origin for frontend", pulled_frontend_master_hash)
            old_pulled_frontend_master_hash = pulled_frontend_master_hash

            # print out for new hash for rosenodemock
            print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), " Check master origin for mockup", pulled_mockup_master_hash)
            old_pulled_mockup_master_hash = pulled_mockup_master_hash

            # print out for new hash for rosenodemock.frontend
            print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), " Check master origin for mockup", pulled_mockup_frontend_master_hash)
            old_pulled_mockup_frontend_master_hash = pulled_mockup_frontend_master_hash


            # skip update for the first invalid hash compare
            # if actual_backend_master_hash is "":
            #    actual_backend_master_hash = pulled_backend_master_hash
            # if actual_frontend_master_hash is "":
            #    actual_frontend_master_hash = pulled_frontend_master_hash

            repos = {}
            repos['roseguarden.backend'] = {
                'old_hash': actual_backend_master_hash,
                'new_hash': pulled_backend_master_hash,
                'update': actual_backend_master_hash != pulled_backend_master_hash
            }
            repos['roseguarden.frontend'] = {
                'old_hash': actual_frontend_master_hash,
                'new_hash': pulled_frontend_master_hash,
                'update': actual_frontend_master_hash != pulled_frontend_master_hash
            }
            repos['rosenodemock'] = {
                'old_hash': actual_mockup_master_hash,
                'new_hash': pulled_mockup_master_hash,
                'update': actual_mockup_master_hash != pulled_mockup_master_hash
            }       
            repos['rosenodemock.frontend'] = {
                'old_hash': actual_mockup_frontend_master_hash,
                'new_hash': pulled_mockup_frontend_master_hash,
                'update': actual_mockup_frontend_master_hash != pulled_mockup_frontend_master_hash
            }                

            if ((actual_backend_master_hash != pulled_backend_master_hash) or 
                (actual_frontend_master_hash != pulled_frontend_master_hash)):
                need_update = True

            if ((actual_mockup_master_hash != pulled_mockup_master_hash) or 
                (actual_mockup_frontend_master_hash != pulled_mockup_frontend_master_hash)):
                need_update = True

            if need_update == True:
                # store repo informations
                with open('../repositories/repositories.txt', 'w') as outfile:
                    json.dump(repos, outfile,indent=4)

                # update the updater            
                updateRoseupdate(supervisor_url, base_local_repo_path + 'roseupdate')

                if (updateRoseguarden(supervisor_url) is True):
                    print("Update finished")
                    actual_backend_master_hash = pulled_backend_master_hash
                    actual_frontend_master_hash = pulled_frontend_master_hash
                    actual_mockup_master_hash = pulled_mockup_master_hash                
                else:
                    print("Update failed")

                need_update = False
        except Exception as e:
            print("Update cycle failed with", str(e))

        time.sleep(180)

repo_backend, repo_frontend, repo_mockup, repo_mockup_frontend = setup()
check_for_updates(repo_backend, repo_frontend, repo_mockup, repo_mockup_frontend)