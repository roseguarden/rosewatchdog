import os
import shutil
from git import Repo, exc


def is_git_repo(path):
    try:
        _ = Repo(path).git_dir
        return True
    except:
        return False

def get_or_create_repo(repository_remote_url, repository_local_path):
    print("try to get ", repository_remote_url)
    try:
        if (is_git_repo(repository_local_path) is False):
            if os.path.exists(repository_local_path) and os.path.isdir(repository_local_path):
                shutil.rmtree(repository_local_path)
            try:
                repo = Repo.clone_from(repository_remote_url, repository_local_path, depth=1)
                return repo
            except (exc.NoSuchPathError, exc.InvalidGitRepositoryError) as e:
                print("git repo git couldn't be cloned", e)
                exit(1)
        else:
            print("repo is fine")
            repo = Repo(repository_local_path)
            return repo
    except Exception as e:
        print("error while getting the repository ({})".format(e))
        try:
            repo = Repo.clone_from(repository_remote_url, repository_local_path, depth=1)
            return repo
        except (exc.NoSuchPathError, exc.InvalidGitRepositoryError) as e:
            print("git repo couldn't be cloned", e)
            exit(1)