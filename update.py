from xmlrpc.client import ServerProxy
from repoTools import is_git_repo
from git import Repo
import time

#server = ServerProxy(('http://localhost:9001/RPC2')

def waitForRoseupdateToFinish(server):
    while True:
        roseupdate_state = server.supervisor.getProcessInfo('roseupdate')['state']
        if (roseupdate_state is 0): # STOPPED state
            break
        if (roseupdate_state is 100): # EXITED state
            break
        print("roseupdate is still running, waiting to close, state =", roseupdate_state)
        time.sleep(2)

def updateRoseupdate(server_url, roseupdate_repository_local_path):
    print("connect to supervisor")
    server = ServerProxy(server_url)

    print("check for roseupdate is still working")
    waitForRoseupdateToFinish(server)

    try:
        if (is_git_repo(roseupdate_repository_local_path) == True):
            print("pull a update for roseupdate")
            repo_roseupdate = Repo(roseupdate_repository_local_path)
            ori = repo_roseupdate.remotes.origin
            ori.pull()
        else:
            print("unable to update roseupdate because repo dosen't exist ")
    except Exception as e:
        print("unable to update roseupdate with error ({})".format(e))

# start roseupdate process
# return:   True for success
#           False for failure 
def updateRoseguarden(server_url):
    print("connect to supervisor")
    server = ServerProxy(server_url)

    print("check for roseupdate is still running")
    waitForRoseupdateToFinish(server)
    
    print("stop roseguarden")
    roseguarden_state = server.supervisor.getProcessInfo('roseguarden')['state']
    if roseguarden_state != 0:
        try:
            server.supervisor.stopProcess('roseguarden')
        except Exception as e:
            print ("stop roseguarden failed with:", str(e))
    rosenodemock_state = server.supervisor.getProcessInfo('rosenodemock')['state']
    if roseguarden_state != 0:
        try:
            server.supervisor.stopProcess('rosenodemock')
        except Exception as e:
            print ("stop rosenodemock failed with:", str(e))

    timeout = 20
    while True:
        roseguarden_state = server.supervisor.getProcessInfo('roseguarden')['state']
        rosenodemock_state = server.supervisor.getProcessInfo('rosenodemock')['state']

        rosenodemock_stopped = False
        roseguarden_stopped = False

        if (roseguarden_state is 0): # STOPPED state
            roseguarden_stopped = True
        if (roseguarden_state is 100): # EXITED state
            roseguarden_stopped = True
        if (roseguarden_state is 200): # FATAL state
            roseguarden_stopped = True

        if (rosenodemock_state is 0): # STOPPED state
            rosenodemock_stopped = True
        if (rosenodemock_state is 100): # EXITED state
            rosenodemock_stopped = True
        if (rosenodemock_state is 200): # FATAL state
            rosenodemock_stopped = True            

        # only continue when both apps stoped
        if (roseguarden_stopped == True and rosenodemock_stopped == True):
            break

        print("roseguarden or rosenodemock is still running, waiting for finish, (roseguarden ={} rosenodemock={})".format(roseguarden_state, rosenodemock_state))
        time.sleep(2)
        timeout -= 1
        if timeout == 0:
            return False

    print("start the roseupdate process")
    server.supervisor.startProcess('roseupdate')

    timeout = 20
    while True:
        roseupdate_state = server.supervisor.getProcessInfo('roseupdate')['state']
        if roseupdate_state == 20:
            print("roseupdate started, state=", roseupdate_state)
            break
        print("wait for roseupdate to be started, state=", roseupdate_state)
        time.sleep(2)
        timeout -= 1
        if timeout == 0:
            return False

    while server.supervisor.getProcessInfo('roseupdate')['state'] is not 0:
        print("wait for roseupdate to be finished, (state ={})".format(server.supervisor.getProcessInfo('roseupdate')['state']))
        time.sleep(2)

    print("restart roseguarden")
    server.supervisor.startProcess('roseguarden')
    print("restart rosenodemock")
    server.supervisor.startProcess('rosenodemock')
    return True
